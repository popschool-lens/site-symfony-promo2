# popschool-lens/site-symfony-promo2

A Symfony project created on October 9, 2017, 9:46 am.

## Install

    git clone https://framagit.org/popschool-lens/site-symfony-promo2.git
    cd site-symfony-promo2
    composer install
    cd web
    bower install

## Doc

- [Symfony 3.3 Documentation](http://symfony.com/doc/current/index.html)
- [5. Association Mapping — Doctrine 2 ORM 2 documentation](http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/association-mapping.html)
- [Class Doctrine\Common\Collections\ArrayCollection | Collections](http://www.doctrine-project.org/api/collections/1.3/class-Doctrine.Common.Collections.ArrayCollection.html)
- [DoctrineMigrationsBundle (The Symfony Bundles Documentation)](http://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html)
- [Form Types Reference (The Symfony Reference)](http://symfony.com/doc/current/reference/forms/types.html)
- [Databases and the Doctrine ORM (current)](http://symfony.com/doc/current/doctrine.html#querying-for-objects-using-doctrine-s-query-builder)
