<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('titre')
          ->add('dateParution')
          ->add('auteur')
          ->add('contenu')
          ->add('publie')
          ->add('categorie', EntityType::class, array(
              'class' => 'AppBundle:Categorie',
              'choice_label' => 'titre',
              'multiple' => false,
              'expanded' => false,
          ))
          ->add('etiquettes', EntityType::class, array(
              'class' => 'AppBundle:Etiquette',
              'choice_label' => 'titre',
              'multiple' => true,
              'expanded' => true,
          ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Article'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_article';
    }


}
