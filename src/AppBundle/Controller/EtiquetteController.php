<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Etiquette;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Etiquette controller.
 *
 * @Route("admin/etiquette")
 */
class EtiquetteController extends Controller
{
    /**
     * Lists all etiquette entities.
     *
     * @Route("/", name="etiquette_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $etiquettes = $em->getRepository('AppBundle:Etiquette')->findAll();

        return $this->render('etiquette/index.html.twig', array(
            'etiquettes' => $etiquettes,
        ));
    }

    /**
     * Creates a new etiquette entity.
     *
     * @Route("/new", name="etiquette_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $etiquette = new Etiquette();
        $form = $this->createForm('AppBundle\Form\EtiquetteType', $etiquette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($etiquette);
            $em->flush();

            return $this->redirectToRoute('etiquette_show', array('id' => $etiquette->getId()));
        }

        return $this->render('etiquette/new.html.twig', array(
            'etiquette' => $etiquette,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a etiquette entity.
     *
     * @Route("/{id}", name="etiquette_show")
     * @Method("GET")
     */
    public function showAction(Etiquette $etiquette)
    {
        $deleteForm = $this->createDeleteForm($etiquette);

        return $this->render('etiquette/show.html.twig', array(
            'etiquette' => $etiquette,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing etiquette entity.
     *
     * @Route("/{id}/edit", name="etiquette_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Etiquette $etiquette)
    {
        $deleteForm = $this->createDeleteForm($etiquette);
        $editForm = $this->createForm('AppBundle\Form\EtiquetteType', $etiquette);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('etiquette_edit', array('id' => $etiquette->getId()));
        }

        return $this->render('etiquette/edit.html.twig', array(
            'etiquette' => $etiquette,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a etiquette entity.
     *
     * @Route("/{id}", name="etiquette_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Etiquette $etiquette)
    {
        $form = $this->createDeleteForm($etiquette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($etiquette);
            $em->flush();
        }

        return $this->redirectToRoute('etiquette_index');
    }

    /**
     * Creates a form to delete a etiquette entity.
     *
     * @param Etiquette $etiquette The etiquette entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Etiquette $etiquette)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('etiquette_delete', array('id' => $etiquette->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
