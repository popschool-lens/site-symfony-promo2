<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateParution", type="datetime")
     */
    private $dateParution;

    /**
     * @var string
     *
     * @ORM\Column(name="auteur", type="string", length=255)
     */
    private $auteur;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    /**
     * @var bool
     *
     * @ORM\Column(name="publie", type="boolean")
     */
    private $publie;

    /**
     * Many Articles have One Categorie.
     * @ORM\ManyToOne(targetEntity="Categorie", inversedBy="articles")
     * @ORM\JoinColumn(name="categorie_id", referencedColumnName="id")
     */
    private $categorie;

    /**
     * Many Articles have Many Etiquettes.
     * @ORM\ManyToMany(targetEntity="Etiquette", mappedBy="articles")
     */
    private $etiquettes;

    public function __construct() {
        $this->etiquettes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Article
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set dateParution
     *
     * @param \DateTime $dateParution
     *
     * @return Article
     */
    public function setDateParution($dateParution)
    {
        $this->dateParution = $dateParution;

        return $this;
    }

    /**
     * Get dateParution
     *
     * @return \DateTime
     */
    public function getDateParution()
    {
        return $this->dateParution;
    }

    /**
     * Set auteur
     *
     * @param string $auteur
     *
     * @return Article
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur
     *
     * @return string
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Article
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set publie
     *
     * @param boolean $publie
     *
     * @return Article
     */
    public function setPublie($publie)
    {
        $this->publie = $publie;

        return $this;
    }

    /**
     * Get publie
     *
     * @return bool
     */
    public function getPublie()
    {
        return $this->publie;
    }

    /**
     * Get Categorie
     *
     * @return Categorie
     */
    public function getCategorie()
    {
      return $this->categorie;
    }

    /**
     * Set Categorie
     *
     * @param Categorie $categorie
     * @return Article
     */
    public function setCategorie(Categorie $categorie)
    {
      $this->categorie = $categorie;

      return $this;
    }

    /**
     * Get Etiquettes
     *
     * @return ArrayCollection
     */
    public function getEtiquettes()
    {
      return $this->etiquettes;
    }

    /**
     * Set Etiquettes
     *
     * @param ArrayCollection $etiquettes
     * @return Article
     */
    public function setEtiquettes(ArrayCollection $etiquettes)
    {
      $this->etiquettes = $etiquettes;

      return $this;
    }

    /**
     * Add Etiquette
     *
     * @param Etiquette $etiquette
     * @return Article
     */
    public function addEtiquette(Etiquette $etiquette)
    {
      $this->etiquettes->add($etiquette);

      return $this;
    }

    /**
     * Remove Etiquette
     *
     * @param Etiquette $etiquette
     * @return Article
     */
    public function removeEtiquette(Etiquette $etiquette)
    {
      $this->etiquettes->removeElement($etiquette);

      return $this;
    }
}
