<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Categorie
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategorieRepository")
 */
class Categorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, unique=true)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * One Categorie has Many Articles.
     * @ORM\OneToMany(targetEntity="Article", mappedBy="categorie")
     */
    private $articles;

    public function __construct() {
        $this->articles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Categorie
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Categorie
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get Articles
     *
     * @return ArrayCollection
     */
    public function getArticles()
    {
      return $this->articles;
    }

    /**
     * Set Articles
     *
     * @param ArrayCollection $articles
     * @return Categorie
     */
    public function setArticles(ArrayCollection $articles)
    {
      $this->articles = $articles;

      return $this;
    }

    /**
     * Add Article
     *
     * @param Article $article
     * @return Categorie
     */
    public function addArticle(Article $article)
    {
      $this->articles->add($article);

      return $this;
    }

    /**
     * Remove Article
     *
     * @param Article $article
     * @return Categorie
     */
    public function removeArticle(Article $article)
    {
      $this->articles->removeElement($article);

      return $this;
    }
}
