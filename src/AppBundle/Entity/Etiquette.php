<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Etiquette
 *
 * @ORM\Table(name="etiquette")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EtiquetteRepository")
 */
class Etiquette
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, unique=true)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * Many Etiquettes have Many Articles.
     * @ORM\ManyToMany(targetEntity="Article", inversedBy="etiquettes")
     * @ORM\JoinTable(name="articles_etiquettes")
     */
    private $articles;

    public function __construct() {
        $this->articles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Etiquette
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Etiquette
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get Articles
     *
     * @return ArrayCollection
     */
    public function getArticles()
    {
      return $this->articles;
    }

    /**
     * Set Articles
     *
     * @param ArrayCollection $articles
     * @return Etiquette
     */
    public function setArticles(ArrayCollection $articles)
    {
      $this->articles = $articles;

      return $this;
    }

    /**
     * Add Article
     *
     * @param Article $article
     * @return Etiquette
     */
    public function addArticle(Article $article)
    {
      $this->articles->add($article);

      return $this;
    }

    /**
     * Remove Article
     *
     * @param Article $article
     * @return Etiquette
     */
    public function removeArticle(Article $article)
    {
      $this->articles->removeElement($article);

      return $this;
    }
}
